-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11 Jul 2021 pada 16.55
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses_manajemens`
--

CREATE TABLE `akses_manajemens` (
  `id_hak_akses` int(11) NOT NULL,
  `nama_hak_akses` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `akses_manajemens`
--

INSERT INTO `akses_manajemens` (`id_hak_akses`, `nama_hak_akses`) VALUES
(1, 'admin'),
(2, 'helper');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `customers`
--

INSERT INTO `customers` (`id`, `email`, `name`, `active`) VALUES
(1, 'Hikmat6@gmail.com', 'hikmat', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dares`
--

CREATE TABLE `dares` (
  `id_dares` int(11) NOT NULL,
  `nama_dares` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `wilayah` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `dares`
--

INSERT INTO `dares` (`id_dares`, `nama_dares`, `alamat`, `wilayah`) VALUES
(1, 'Kampung Cikole', 'Lembang', 'Kab. Bandung Barat'),
(2, 'kampung Suntenjaya', 'Lembang', 'Kab. Bandung Barat'),
(3, 'Kampung Cikidang', 'Lembang', 'Kab. Bandung Barat'),
(4, 'Kampung Sukatani', 'Lembang', 'Kab. Bandung Barat'),
(5, 'Kampung Sekeangkrih', 'Cileunyi', 'Kab. Bandung'),
(6, 'Kampung Cibeusi', 'Cileunyi', 'Kab. Bandung'),
(7, 'Kampung Cijati', 'Cileunyi', 'Kab. Bandung'),
(8, 'Kampung Cisitu', 'Cileunyi', 'Kab. Bandung'),
(9, 'Kampung Cibiru beet', 'Cileunyi', 'Kab. Bandung'),
(10, 'Kampung Babakan Pandan', 'Cileunyi', 'Kab. Bandung'),
(11, 'Kampung Cijantung', 'Purwakarta', 'Kab. Purwakarta'),
(12, 'Kampung Saguling', 'Purwakarta', 'Kab. Purwakarta'),
(13, 'Pesantren Alkasyaf', 'Sekecariu Cileunyi', 'Kab. Bandung'),
(14, 'Pesantren Nurul Jannah', 'Dago Giri', 'Kab. Bandung Barat'),
(15, 'Pesantren Yatim Al- Aziz', 'Kampung Cijambu', 'Kab. Sumedang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `donaturs`
--

CREATE TABLE `donaturs` (
  `id_donatur` int(11) NOT NULL,
  `nama_donatur` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telepon` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `atas_nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `donaturs`
--

INSERT INTO `donaturs` (`id_donatur`, `nama_donatur`, `email`, `telepon`, `alamat`, `atas_nama`) VALUES
(1, 'Dendi', '', '085085085', 'Bandung', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hewans`
--

CREATE TABLE `hewans` (
  `id_hewan` int(11) NOT NULL,
  `kode_hewan` varchar(50) NOT NULL,
  `jenis_hewan` varchar(50) NOT NULL,
  `harga_hewan` int(20) DEFAULT NULL,
  `foto` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `bobot` varchar(100) DEFAULT NULL,
  `stok` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `hewans`
--

INSERT INTO `hewans` (`id_hewan`, `kode_hewan`, `jenis_hewan`, `harga_hewan`, `foto`, `video`, `bobot`, `stok`) VALUES
(1, 'DB001', 'Domba', 2600000, '/images/domba1.jpg', '/videos/domba1.mp4', '35', 1),
(2, 'DB002', 'Domba', 3000000, 'dombakelasC.jpeg', 'videodombakelasC.mp4', '42 kg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `qurbans`
--

CREATE TABLE `qurbans` (
  `id_faktur` int(11) NOT NULL,
  `kode_faktur` varchar(100) NOT NULL,
  `id_donatur` int(11) NOT NULL,
  `id_hewan` int(11) NOT NULL,
  `id_helper` int(11) DEFAULT NULL,
  `id_statusSalur` int(11) DEFAULT NULL,
  `id_dares` int(11) DEFAULT NULL,
  `alamat_kirim` varchar(255) DEFAULT NULL,
  `minta_bagian` varchar(100) DEFAULT NULL,
  `metode_bayar` varchar(100) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `id_statusTrx` int(11) NOT NULL,
  `update_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `qurbans`
--

INSERT INTO `qurbans` (`id_faktur`, `kode_faktur`, `id_donatur`, `id_hewan`, `id_helper`, `id_statusSalur`, `id_dares`, `alamat_kirim`, `minta_bagian`, `metode_bayar`, `tgl_bayar`, `id_statusTrx`, `update_by`) VALUES
(1, 'SQA001', 1, 1, 0, 1, 1, '', '', 'Transfer BCA', '0000-00-00', 1, 0),
(2, 'SQA002', 1, 2, NULL, NULL, NULL, NULL, NULL, 'Transfer BCA', '0000-00-00', 2, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `shodaqohs`
--

CREATE TABLE `shodaqohs` (
  `id_kupon` int(11) NOT NULL,
  `kode_kupon` varchar(255) NOT NULL,
  `id_donatur` int(11) NOT NULL,
  `nominal` varchar(255) NOT NULL,
  `metode_bayar` varchar(255) NOT NULL,
  `bukti_bayar` varchar(255) DEFAULT NULL,
  `tgl_bayar` date NOT NULL,
  `id_statusTrx` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `shodaqohs`
--

INSERT INTO `shodaqohs` (`id_kupon`, `kode_kupon`, `id_donatur`, `nominal`, `metode_bayar`, `bukti_bayar`, `tgl_bayar`, `id_statusTrx`) VALUES
(1, 'SQ-001', 1, '100000', 'Transfer BCA', '', '2021-07-11', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_salurs`
--

CREATE TABLE `status_salurs` (
  `id_statusSalur` int(11) NOT NULL,
  `kode_statusSalur` varchar(255) NOT NULL,
  `detail_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `status_salurs`
--

INSERT INTO `status_salurs` (`id_statusSalur`, `kode_statusSalur`, `detail_status`) VALUES
(1, 'ND', 'Salur Penuh'),
(2, 'NC', 'Salur Minta Bagian'),
(3, 'Non', 'PHQ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_transaksis`
--

CREATE TABLE `status_transaksis` (
  `id_statusTrx` int(11) NOT NULL,
  `kode_statusTrx` varchar(100) NOT NULL,
  `detail_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `status_transaksis`
--

INSERT INTO `status_transaksis` (`id_statusTrx`, `kode_statusTrx`, `detail_status`) VALUES
(1, 'Belum Lunas', 'Menunggu Pembayaran'),
(2, 'Upload BT', 'Sudah Upload Pembayaran'),
(3, 'Lunas', 'Confrimed');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `name`, `email`, `username`, `hash`) VALUES
(3, 'Hikmat Hidayat', 'hikmat6@gmail.com', 'hikmat6', '$2a$10$xZfqsoGhvuoeeEYI794vlOBn9YT0WJoKpnAzY/VFaK4oVztqT7ugK'),
(4, 'toga', 'togarong@gmail.com', 'togarong', '$2a$10$.netYboNleiAtR0RhGe7ZugeKOtOo71My.kEVLBbWGSoRrjBvdZn6'),
(5, 'ocha ocha', 'ichiocha@gmail.com', 'ichiocha', '$2a$10$9De5iE0N38CoxQH9oULwj.UdA4s1XiB0PhEC.7hJY.rabXjhnV.Yu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses_manajemens`
--
ALTER TABLE `akses_manajemens`
  ADD PRIMARY KEY (`id_hak_akses`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dares`
--
ALTER TABLE `dares`
  ADD PRIMARY KEY (`id_dares`);

--
-- Indexes for table `donaturs`
--
ALTER TABLE `donaturs`
  ADD PRIMARY KEY (`id_donatur`);

--
-- Indexes for table `hewans`
--
ALTER TABLE `hewans`
  ADD PRIMARY KEY (`id_hewan`);

--
-- Indexes for table `qurbans`
--
ALTER TABLE `qurbans`
  ADD PRIMARY KEY (`id_faktur`);

--
-- Indexes for table `shodaqohs`
--
ALTER TABLE `shodaqohs`
  ADD PRIMARY KEY (`id_kupon`);

--
-- Indexes for table `status_salurs`
--
ALTER TABLE `status_salurs`
  ADD PRIMARY KEY (`id_statusSalur`);

--
-- Indexes for table `status_transaksis`
--
ALTER TABLE `status_transaksis`
  ADD PRIMARY KEY (`id_statusTrx`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses_manajemens`
--
ALTER TABLE `akses_manajemens`
  MODIFY `id_hak_akses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dares`
--
ALTER TABLE `dares`
  MODIFY `id_dares` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `donaturs`
--
ALTER TABLE `donaturs`
  MODIFY `id_donatur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hewans`
--
ALTER TABLE `hewans`
  MODIFY `id_hewan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `qurbans`
--
ALTER TABLE `qurbans`
  MODIFY `id_faktur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shodaqohs`
--
ALTER TABLE `shodaqohs`
  MODIFY `id_kupon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `status_salurs`
--
ALTER TABLE `status_salurs`
  MODIFY `id_statusSalur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status_transaksis`
--
ALTER TABLE `status_transaksis`
  MODIFY `id_statusTrx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
