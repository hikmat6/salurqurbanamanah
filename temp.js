module.exports = {
    apps: [
      {
        name: 'next-site',
        cwd: ' /home/your-name/my-nextjs-project',
        script: 'npm',
        args: 'start',
        port:'1337',
        env: {
          NEXT_PUBLIC_ : 'NEXT_PUBLIC_...',
        },
      },
      // optionally a second project
  ],
};