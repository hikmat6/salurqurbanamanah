import axios from 'axios';

const apiUrl = process.env.NEXT_PUBLIC_API_HOST;

export const postLogin = async (data) => {
    try {
        const response = await axios.post(apiUrl + 'users/authenticate', data);
        return response;
    } catch (error) {
        throw error;
    }
}

export const postShodaqoh = async (data) => {
    try {
        const response = await axios.post(apiUrl + 'shodaqoh/', data);
        return response;
    } catch (error) {
        throw error;
    }
}

export const getUsersAll = async () => {
    try {
        const response = await axios.get(apiUrl + 'users/');
        return response;
    } catch (error) {
        throw error;
    }
}

