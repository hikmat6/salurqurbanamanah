import React from 'react'

export default function Sajadah() {
    return (
        <section className="text-gray-600 body-font birukhas font-primary">
            <div className="flex flex-wrap justify-center -m-4">
             <img className="object-cover object-center rounded w-4/5 h-4/5" alt="hero" src="/images/Logo Sajadah Tengah.png"/>        
            </div>
            <div className="h-full p-8 rounded text-white">
                   <div className="flex flex-wrap justify-center">
                   <p className="leading-relaxed mb-6 text-lg">
                    "Ada dua yang pahala amalnya tidak akan berkurang. Keduanya dua bulan <br></br>hari raya: Bulan Ramadhan dan Bulan Dzulhijjah."
                    </p>
                   </div>
                   <div className="flex flex-wrap justify-center">
                   <br></br>
                    <a className="inline-flex items-center">   
                        <span className="flex-grow flex flex-col pl-4">
                        <span className="title-font font-medium text-white">(HR. Bukhori 1912 dan Muslim 1089)</span>                        
                        </span>
                    </a>
                </div>
                 
                 </div>

        </section>
    )
}