import React from 'react'

export default function Target() {
    return (
        <section className="text-gray-600 body-font bg-gray-100">
            <div className="container px-5 py-20 mx-auto ">
                <div className="flex flex-wrap -m-4 text-center justify-center">
                <div className="p-4 md:w-2/5 sm:w-1/2 w-full">
                    <div className="border-2 border-gray-200 px-4 py-6 rounded-lg bg-white">                    
                    <h2 className="title-font font-medium text-2xl text-gray-900">Target Daerah sasaran penyaluran: </h2>
                        <div className="flex flex-wrap text-center justify-around mt-2">
                            <div>
                                <h3 className="title-font font-bold text-3xl text-green-600">12</h3>
                                <p className="leading-relaxed">Daerah Binaan</p>
                            </div>
                            <div>
                                <h3 className="title-font font-bold text-3xl text-green-600">700</h3>
                                <p className="leading-relaxed">Mustahiq</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="p-4 md:w-2/5 sm:w-1/2 w-full">
                <div className="border-2 border-gray-200 px-4 py-6 rounded-lg bg-white">                    
                    <h2 className="title-font font-medium text-2xl text-gray-900">Target Pesantren dan Dhuafa sasaran: </h2>
                        <div className="flex flex-wrap text-center justify-around mt-2">
                            <div>
                                <h3 className="title-font font-bold text-3xl text-green-600">3</h3>
                                <p className="leading-relaxed">Pesantren dan dhuafa</p>
                            </div>
                            <div>
                                <h3 className="title-font font-bold text-3xl text-green-600">300</h3>
                                <p className="leading-relaxed">Mustahiq</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="p-4 md:w-1/5 sm:w-1/2 w-full">
                    <div className="border-2 border-gray-200 px-4 py-6 rounded-lg bg-white">
                    <h2 className="title-font font-medium text-2xl text-gray-900">Target Muqorib: </h2>
                    <h3 className="title-font font-bold text-3xl text-green-600">150</h3>
                    <p className="leading-relaxed">Muqorib</p>
                    </div>
                </div>

                </div>
            </div>
            </section>
    )
}
