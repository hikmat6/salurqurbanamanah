import React from 'react'

export default function Galeri() {
    return (
        <section className="text-gray-600 body-font">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col text-center w-full mb-20">
      <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Dokumentasi Salur Qurban Amanah</h1>      
    </div>
    <div className="flex flex-wrap -m-4">
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok10.jpg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-16">Kampung Cijantung, Purwakarta</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok1.jpeg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-12">Rumah Tahfidz Nurul Jannah, Kampung Dago giri, Kab. Bandung Barat</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok8.jpg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-16">Kampung Suntenjaya Kec. Lembang Kab. Bandung Barat</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok13.jpg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-16">Kampung Cikole Kec. Lembang Kab. Bandung Barat</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok4.jpeg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-16">Kampung Sekeangkrih Kec. Cileunyi Kab. Bandung</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok9.jpg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-16">Pemberdayaan Pemuda Desa Cisitu Kec Cileunyi Kab. Bandung</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok12.jpg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-16">Kantor Kecamatan Cileunyi untuk Dhuafa Alkasyaf</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok11.jpg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-16">Kampung Cibeusi Kec. Cileunyi Kab. Bandung</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
      <div className="lg:w-1/3 sm:w-1/2 p-4">
        <div className="flex relative">
          <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="/images/dok6.jpeg"/>
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">Dokumentasi SQA 2020</h2>
            <p className="leading-relaxed mb-16">Pesantren yatim Al-aziz, kecamatan Cijambu, kabupaten Sumedang</p>
            <p className="leading-relaxed"></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
    )
}
