import React from 'react'
import Link from 'next/link'

export default function Footer() {
    return (
        <footer className="text-white body-font hijaukhas font-primary">
        <div className="container px-5 py-16 mx-auto flex justify-between md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
          <div className="w-80 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
            <a className="flex title-font font-medium items-center md:justify-start justify-center text-white">
            <img alt="Logo SQA" className="inset-0 w-auto h-10 object-cover object-center mb-4" src="/images/Logo SQA.png"/>
              <span className="ml-3 text-xl">Salur Qurban Amanah</span>
            </a>
            <br />
            <a className="flex title-font font-medium items-center md:justify-start justify-center text-white">
            <img alt="Logo Arista" className="inset-0 w-auto h-10 object-cover object-center mb-4" src="/images/Logo Arista.png"/>
              <span className="ml-3 text-xl">Yayasan Arista</span>
            </a>
            <br />
            <p className="mt-2 text-sm text-white">SK Menkumham : <span>No. AHU-6307.A.H.01.04 Tahun 2012</span>
            <br /> Akta Notaris : <span>Deni, SH, M.Kn No. 6 Tanggal 06 Februari 2012</span></p>
          </div>
          <div className="flex-grow flex flex-wrap justify-end md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center">
            <div className="lg:w-1/3 md:w-1/2 w-full px-4">
              <h2 className="title-font font-medium text-white tracking-widest text-sm mb-3">Contact Info</h2>
              <br/>
              <p className="text-white">Alamat :</p>
              <p>Jl. SMPN 1 No. 9 Kec. Cileunyi Bandung</p>
              <br/>
              <p>No Contact : </p>
              <p>- <a href="https://api.whatsapp.com/send?phone= 6287724874918&text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.SalurQurbanAmanah,%20saya%20ingin%20bertanya%20mengenai%20qurban" className="underline hover:no-underline hover:text-gray-400"><span> Irfaldi R</span> 0877-2487-4918 </a></p>
              <p>- <a href="https://api.whatsapp.com/send?phone= 6289512516754&text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.SalurQurbanAmanah,%20saya%20ingin%20bertanya%20mengenai%20qurban" className="underline hover:no-underline hover:text-gray-400"><span> Ajie P</span> 0895-1251-6754</a></p>
              <p>- <a href="https://api.whatsapp.com/send?phone= 628112208661&text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.SalurQurbanAmanah,%20saya%20ingin%20bertanya%20mengenai%20qurban" className="underline hover:no-underline hover:text-gray-400"><span> Rangga</span> 0811-2208-661</a></p>
            </div>
            <div className="lg:w-1/3 md:w-1/2 w-full px-4">
              <h2 className="title-font font-medium text-white tracking-widest text-sm mb-3">Tentang</h2>
              <nav className="list-none mb-10">
                <li>
                  <a className="text-white hover:text-gray-800"> Syarat & Ketentuan</a>
                </li>
                <li>
                  <a className="text-white hover:text-gray-800">Tentang Kami</a>
                </li>
                <li>
                  <a className="text-white hover:text-gray-800">Kebijakan Privasi</a>
                </li>
              </nav>
            </div>
            <div className="lg:w-1/3 md:w-1/2 w-full px-4">
              <h2 className="title-font font-medium text-white tracking-widest text-sm mb-3">Pusat Bantuan</h2>
              <nav className="list-none mb-10">
                <li>
                  <a className="text-white hover:text-gray-800">FAQ</a>
                </li>
                <li>
                  <a className="text-white hover:text-gray-800">Hukum Qurban</a>
                </li>
                <li>
                  <a className="text-white hover:text-gray-800">Daftar Pequrban</a>
                </li>         
              </nav>
            </div>
          
          </div>
        </div>
  {/* <div className="bg-green-300">
    <div className="container mx-auto py-4 px-5 flex flex-wrap flex-col sm:flex-row">
      <p className="text-white text-sm text-center sm:text-left">© 2021 Salur Qurban Amanah by Yayasan Arista <Link href="/login"><a>, halaman admin</a></Link>
        
      </p>
      <span className="inline-flex sm:ml-auto sm:mt-0 mt-2 justify-center sm:justify-start">
        <a className="text-white">
          <svg fill="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
            <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
          </svg>
        </a>
        <a className="ml-3 text-white">
          <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
            <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
            <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
          </svg>
        </a>        
      </span>
    </div>
  </div> */}
</footer>
    )
}
