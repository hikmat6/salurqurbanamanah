import React from 'react'

export default function Dimensi() {
    return (
        <section className="text-gray-600 body-font bg-white py-24 font-primary">
            <div className="flex flex-wrap justify-center mt-20">
                <p className="leading-relaxed mr-6 text-5xl text-merahkhas font-bold">
                4 DIMENSI<br></br>KEBAIKAN<br></br>SQA SAE <br></br>PISAN !                
                </p>
             <img className="object-cover object-center rounded w-2/5 h-2/5" alt="hero" src="/images/4 Dimensi.png"/>        
            </div>
        </section>
    )
}