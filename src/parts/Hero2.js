import React from 'react'

export default function Hero2() {
    return (
        <section className="body-font bg-white text-black font-primary">
        <div className="container mx-auto flex px-5 md:flex-row flex-col items-center z-50">
      <div className="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <img className="object-cover object-center rounded h-24" alt="hero" src="/images/Logo Sae Pisan.png"/>
      <h2 className="title-font sm:text-4xl text-3xl mb-4 font-bold">         
      </h2>
      <p className="mb-8 font-normal leading-relaxed">Iedul Adha 1444 H merupakan momentum terbaik untuk mendekatkan diri kepada Alloh SWT serta membangun kepedulian sosial dengan sesama.</p>
      <p className="mb-8 font-normal leading-relaxed"><span>Salur qurban amanah </span> diharapkan menjadi jembatan emas untuk pendekatan persuasif kepada masyarakat yang berada di desa melalui penyaluran Qurban yang tepat sasaran.</p>
    </div>
            <div className="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mx-auto my-auto">
            <img className="object-cover object-center rounded" alt="hero" src="/images/Hero03.png"/>
            </div>
        </div>        
        
        </section>
    )
}
