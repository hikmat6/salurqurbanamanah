import React,{useState} from 'react'
import Router from 'next/router';
import {postLogin} from '../helpers/API'

export default function Login() {
  const [form, setForm] = useState({
    username: '',
    password: ''
  });

  const submitLoginApi = async (e) => {
    e.preventDefault();
    console.log(form)
    try {        
        const response = await postLogin(form);
        console.log(response);
        Router.push('/');
    } catch (error) {
      console.log(error);
    }
}

  const updateField = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

    return (
<section className="text-gray-600 bg-white relative">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col text-center w-full mb-12">
      <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Login</h1>      
    </div>
    <div className="lg:w-1/2 md:w-2/3 mx-auto">
      <div className="flex flex-wrap -m-2">
        <div className="p-2 w-full">
          <div className="relative">
            <label htmlFor="name" className="leading-7 text-sm text-gray-600">Username</label>
            <input type="text" name="username" className="w-full bg-blue-300 bg-opacity-50 rounded border border-black focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value={form.username} onChange={updateField}/>
          </div>
        </div>
        <div className="p-2 w-full">
          <div className="relative">
            <label htmlFor="password" className="leading-7 text-sm text-gray-600">Password</label>
            <input type="password" name="password" className="w-full bg-blue-300 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value={form.password} onChange={updateField}/>
          </div>
        </div>
       
        <div className="p-2 w-full">
          <button className="flex mx-auto text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg" onClick={submitLoginApi}>Login</button>
        </div>
        
    </div>
  </div>
  </div>
</section>
    )
}
