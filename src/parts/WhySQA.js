import React from 'react'

export default function WhySQA() {
    return (
        <section className="text-gray-600 body-font kuningkhas font-primary">
            <div className="container px-5 mx-auto">                
                <div className="flex flex-wrap -m-4">
                <div className="md:w-1/2 w-full">
                <div className="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mx-auto my-auto">
                <img className="object-cover object-center rounded" alt="hero" src="/images/BG-SQA2.png"/>
                </div>
                </div>
                <div className="md:w-1/2 w-full">
                    <div className="h-full p-8 rounded">
                    <p className="mb-3 font-black text-5xl text-white">
                        Mengapa <br></br>Harus SQA ?</p>
                    <p className="leading-relaxed">Kondisi di beberapa desa yang masih minim akan jumlah hewan qurban dikarenakan akses yang sulit dan jarak yang jauh, berbanding terbalik dengan kondisi di kota yang jumlah penyembelihan dan penyebaran hewan Qurbannya masif. Kehadiran SQA diharapkan dapat menjembatani kesenjangan antara desa dan kota dalam penyebaran hewan dan daging Qurban. Sehingga dalam pelaksanaan qurban dan keberkahan di Bulan Dzulhijjah dapat terrasakan oleh semua kalangan.</p>
                    <p className="leading-relaxed">Kamipun turut memperhatikan kondisi peternak di desa akan haknya dalam melaksanakan jual beli hewan Qurban yang belakangan ini justru tidak memberikan dampak lebih kepada para peternak. SQA menjadi solusi bagi peternak begitupun bagi Muqorib tentang bagaimana melaksanakan Qurban sesuai syariat.</p>
                    <p className="leading-relaxed mb-6">Tidak banyak yang mengerti dan mengetahui akan kondisi seperti yang disebutkan diatas. Dengan keresahan tersebut kami berupaya turut menggeliatkan para pemuda dalam proses kegiatan penyaluran hewan Qurban. Dengan harapan dapat memperbaiki kondisi Sosial, Aqidah dan Ekonomi di desa maupun kota.</p>

                    </div>
                </div>
                </div>
            </div>
        </section>
    )
}
