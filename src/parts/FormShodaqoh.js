import React,{useState} from 'react'
import {postShodaqoh} from '../helpers/API'

export default function FormShodaqoh() {
  const [form, setForm] = useState({
    nama_donatur :'',
    telepon :'',
    alamat :'',
    nominal :'',
    metode_bayar :''
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(form)

    try {        
        const response = await postShodaqoh(form);
        console.log(response);
    } catch (error) {
      console.log(error);
    }
}

  const handleChange = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

    return (
        <section className="text-gray-600 body-font relative">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col text-center w-full mb-12">
      <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Shodaqoh Qurban</h1>
      <p className="lg:w-2/3 mx-auto leading-relaxed text-base">Walau sedikit dicintai Rosul SWA</p>
    </div>
    <div className="lg:w-1/2 md:w-2/3 mx-auto">
      <div className="flex flex-wrap -m-2">
        <div className="p-2 w-1/2">
          <div className="relative">
            <label htmlFor="name" className="leading-7 text-sm text-gray-600">Nama</label>
            <input type="text" id="nama_donatur" name="nama_donatur" className="w-full bg-blue-300 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value={form.name} onChange={handleChange}/>
           
          </div>
        </div>
        <div className="p-2 w-1/2">
          <div className="relative">
            <label htmlFor="telepon" className="leading-7 text-sm text-gray-600">Telepon</label>
            <input type="text" id="telepon" name="telepon" className="w-full bg-blue-300 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value={form.telepon} onChange={handleChange}/>
          </div>
        </div>
        <div className="p-2 w-full">
          <div className="relative">
            <label htmlFor="Alamat" className="leading-7 text-sm text-gray-600">Alamat</label>
            <textarea id="alamat" name="alamat" className="w-full bg-blue-300 bg-opacity-50 rounded border border-indigo-500 focus:border-red-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out" value={form.alamat} onChange={handleChange}/>
          </div>
        </div>
        <div className="p-2 w-full">
          <div className="relative">
            <label htmlFor="nominal" className="leading-7 text-sm text-gray-600">Nominal</label>            
            <input type="text" id="nominal" name="nominal" className="w-full bg-blue-300 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value={form.nominal} onChange={handleChange}/>
          </div>
        </div>
        <div className="p-2 w-full">
          <div className="relative">
            <label htmlFor="metode_bayar" className="leading-7 text-sm text-gray-600">Metode bayar</label>            
            <input type="text" id="metode_bayar" name="metode_bayar" className="w-full bg-blue-300 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value={form.metode_bayar} onChange={handleChange}/>
          </div>
        </div>
        <div className="p-2 w-full">
          <button className="flex mx-auto text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg" onClick={handleSubmit} >Tunaikan Shodaqoh Qurban</button>
        </div>
        
      </div>
    </div>
  </div>
</section>
    )
}
