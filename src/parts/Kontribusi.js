import React from 'react'

export default function Kontribusi() {
    return (
        <section className="text-gray-600 body-font font-primary">

<p className="mb-3 font-black text-5xl text-hijaukhas flex flex-wrap justify-center ">Bagaimana Cara Kontribusinya ?</p>
<br></br>
        <div className='flex flex-wrap justify-center'>
        <div className="max-w-sm p-6 mr-6 border border-gray-200 rounded-lg shadow kuningkhas flex flex-wrap justify-center">
            <img className="object-cover object-center rounded w-1/2 h-1/2" alt="hero" src="/images/Logo Domba.png"/>            
            <p className="mb-3 font-bold text-xl text-white flex flex-wrap justify-center">Salur Qurban Amanah</p>            
        </div>
        <div className="max-w-sm p-6 mr-6 border border-gray-200 rounded-lg shadow hijaukhas flex flex-wrap justify-center">
            <img className="object-cover object-center rounded w-1/2 h-1/2" alt="hero" src="/images/Logo Shadaqoh.png"/>   
            <p className="mb-3 font-bold text-xl text-white flex flex-wrap justify-center"><br></br> Shodaqoh Dzulhijjah</p>            
        </div>
        <div className="max-w-sm p-6 mr-6 border border-gray-200 rounded-lg shadow kuningkhas flex flex-wrap justify-center">
            <img className="object-cover object-center rounded w-1/2 h-1/2" alt="hero" src="/images/Logo Wakaf Quran.png"/>            
            <p className="mb-3 font-bold text-xl text-white flex flex-wrap justify-center">.... Wakaf Qur'an ....</p>            
        </div>
        </div>    
        
        </section>
    )
}