import React from 'react'

export default function KontakPerson() {

    return (
        <section className="text-gray-600 body-font font-primary">

<p className="mb-3 font-black text-5xl text-hijaukhas flex flex-wrap justify-center ">Salurkan Qurban &<br></br>Shodaqoh Dzulhijjah<br></br>Terbaik Anda</p>
<br></br>
        <div className='flex flex-wrap justify-center'>
        <img className="object-cover object-center rounded w-1/3 h-1/3" alt="hero" src="/images/Kontak Person.png"/>
        </div>    
        
        </section>
    )
}