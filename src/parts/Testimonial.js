import React from 'react'

export default function Testimonial() {
    return (
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-24 mx-auto">
        <h1 className="text-3xl font-medium title-font text-gray-900 mb-12 text-center">Testimonials</h1>
          <div className="flex flex-wrap -m-4">
            <div className="lg:w-1/3 lg:mb-0 mb-6 p-4 ">
              <div className="h-full text-center bg-gray-100 p-8 rounded">          
                <p className="leading-relaxed">Praktis, Terpercaya</p>
                <span className="inline-block h-1 w-10 rounded bg-indigo-500 mt-6 mb-4"></span>
                <h2 className="text-gray-900 font-medium title-font tracking-wider text-sm">Drg. Lucky Ratna Novianti</h2>
                <p className="text-gray-500">Donatur</p>
              </div>
            </div>
            <div className="lg:w-1/3 lg:mb-0 mb-6 p-4 ">
              <div className="h-full text-center bg-gray-100 p-8 rounded">          
                <p className="leading-relaxed">Alhamdulillah dengan program Salur Qurban Amanah (SQA) ini saya beserta warga di Kampung Cibeusi ini merasakan manfaatnya dimana Salur Qurban Amanah (SQA) membeli hewan qurban dari salah satu peternak lokal di kampung kami, selain itu kami juga mendapatkan daging dari program Salur Qurban Amanah (SQA) ini.</p>
                <span className="inline-block h-1 w-10 rounded bg-indigo-500 mt-6 mb-4"></span>
                <h2 className="text-gray-900 font-medium title-font tracking-wider text-sm">Bapak Asep</h2>
                <p className="text-gray-500">Warga Kampung Cibeusi Kab. Bandung</p>
              </div>
            </div>
            <div className="lg:w-1/3 lg:mb-0 p-4 ">
              <div className="h-full text-center bg-gray-100 p-8 rounded">          
                <p className="leading-relaxed">Program Salur Qurban Amanah (SQA) itu Mudah dan Sangat Akomodatif, serta Pelayanan dari SQA Baik Sekali</p>
                <span className="inline-block h-1 w-10 rounded bg-indigo-500 mt-6 mb-4"></span>
                <h2 className="text-gray-900 font-medium title-font tracking-wider text-sm">Dr. Athiyah Sulistiani . Sp. Ot</h2>
                <p className="text-gray-500">Donatur</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
}
