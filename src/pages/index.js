import Head from 'next/head'
import Hero from '../parts/Hero'
import Hero2 from '../parts/Hero2'
import Quotes from '../parts/Quotes'
import WhySQA from '../parts/WhySQA'
import Sajadah from '../parts/Sajadah'
import Dimensi from '../parts/Dimensi'
import ListHewan from '../parts/ListHewan'
import Galeri from '../parts/Galeri'
import Testimonial from '../parts/Testimonial'
import Kontribusi from '../parts/Kontribusi'
import Dares from '../parts/Dares'
import PilihanHarga from '../parts/PilihanHarga'
import KontakPerson from '../parts/KontakPerson'
import Target from '../parts/Target'
import Footer from '../parts/Footer'


export default function Home() {
  return (
    <div>
      <Head>
        <title>Salur Qurban Amanah</title>
        <meta name="description" content="Salur Qurban Amanah 2021" />
        <link rel="icon" href="/images/Logo SQA.png" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"></link>

        
      </Head>
      <main>
        <Hero2></Hero2>
        
        <WhySQA></WhySQA>
        <Sajadah></Sajadah>
        <Dimensi></Dimensi>
        <Kontribusi></Kontribusi>
        {/* <Testimonial></Testimonial> */}
        <PilihanHarga></PilihanHarga>
        <Dares></Dares>
        <KontakPerson></KontakPerson>
        {/* <Target></Target> */}
        <Footer></Footer>
      </main>
      
    </div>
  )
}
