import React from 'react'
import Head from 'next/head'
import FormQurban from '../parts/FormQurban'
import FormShodaqoh from '../parts/FormShodaqoh'
import Footer from '../parts/Footer'

export default function halamanFormShodaqoh() {
    return (
        <div>
            <Head>
                <title>Salur Qurban Amanah</title>
                <meta name="description" content="Salur Qurban Amanah 2021" />
                <link rel="icon" href="/images/Logo SQA.png" />
            </Head>
            <main>
                <FormShodaqoh></FormShodaqoh>
                <Footer></Footer>
            </main>
        </div>
    )
}
