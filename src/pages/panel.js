import React from 'react'
import Head from 'next/head'
import Footer from '../parts/Footer'

export default function login() {
    return (
        <div>
        <Head>
            <title>Salur Qurban Amanah</title>
            <meta name="description" content="Salur Qurban Amanah 2021" />
            <link rel="icon" href="/images/Logo SQA.png" />
        </Head>
        <main>
            
            <Footer></Footer>
        </main>
        </div>
    )
}
